"""
test theapp
setup: go into theapplication dir and execute: 
pip install -e .  

This will install a dynamically updated package of the app being developed
"""

import theapp

def test_repeated_string_to_print():
    assert theapp.repeated_string_to_print() == 3
