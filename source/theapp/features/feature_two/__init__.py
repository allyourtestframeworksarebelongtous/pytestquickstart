
feature2 = "I am Feature Two in featuretwo"

def return_feature2():
    return "I return a part of Feature Two in __init__.py but I am not covered in a unit test"

def function_not_tested():
    a_string = "give me all the things"
    print(a_string)
    return a_string