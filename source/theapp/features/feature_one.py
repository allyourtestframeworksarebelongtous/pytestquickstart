
from math import sqrt

feature1 = "I am Feature One in feature_one.py"

def return_feature1():
    return "I return a part of Feature One in feature_one.py but I am not covered in a unit test"

def feature1function(a, b):
    """
    I return the lenth of a 3rd side of a right triangle
    """
    return sqrt(a**2 + b**2)
    