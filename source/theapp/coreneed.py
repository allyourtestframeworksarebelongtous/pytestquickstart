
def repeated_string_to_print(string_to_print="Hello World", repeated_count=3):
    count = 0
    for _ in range(repeated_count):
        print(f"{string_to_print}: {_}")
        count += 1
    return count
