# pytestquickstart

This is intended to be a quick start repo for pytest but also serve as a collection of examples should you want to dive deeper.

## TOC

- [Quick Setup and Run](#instructions-for-a-quick-run-of-pytest)
- [Pytest Fixtures](./aux_tests/README_fixtures_explained.md)
- Quick Start examples:
  - [fixtures](./aux_tests/README_fixtures.md)
  - [params](./aux_tests/README_fixture_params.md)
  - [autouse](./aux_tests/README_autouse.md)

Suggestion: follow setup instructions

## Instructions for a quick run of pytest

- clone this repo
- (optional) setup and activate venv (if using venv, be sure its istalled if not already included)
  - `py -3 -m venv .venv`
  - `python3 -m venv .venv`
  - linux: `source .venv/bin/activate`
  - pwsh: `.\.venv\Scripts\Activate.ps1`
- install test dependencies
  - win: `python -m pip install --user -U -r test_requirements.txt`
  - debian: `python3 -m pip install -U -r test_requirements.txt`
  - `pip install -U -r test_requirements.txt`
- install the dummy package in the venv so it can be imported
  - go into `.\source` dir and execute: `pip install -e .`
  - _Note: This will install a dynamically updated package of the app being developed_
- change into any directory in which:
  - the current or child directories have "test_" files with functions that have "test_"
  - the directory contains a `pytest.ini` file that provides extra execution instructions (see later in this document for more details)
- execute:
  - `pytest`
  - verbose: `pytest -v`
  - select tests based on name `pytest -k feature -v`

Note: 
  - if using linux and the the python virtual environment is ***NOT*** installed, you will require sudo/admin to install globally.  
  - If installed locally, the `pytest` executable may need to be added to path.

### Suggestions with integrating pytest with project:

- have a requirements.txt with versions frozen when going to production
- naming convention
    - put tests in the the "tests" dir
    - use the same dir structure as the project
    - name every test file the same as the associated python file in the project used but have it prefixed with "test"
        - Example: `theapp` as a dir would have an `__init__.py` and the tests would be in `tests/test_theapp.py`
        - Example: `theapp/coreneed.py` as a file would have pytests located in `tests/test_myapp_coreneed.py`
        - Example: `theapp/features` as a dir would have an `__init__.py` and the tests would be in `tests/test_theapp_features.py`
        - Example: `theapp/features/featuretwo` as a dir would have an `__init__.py` and the tests would be in `tests/test_theapp_features_featuretwo.py`
        - Example: `theapp/features/feature_one.py` as a file would have pytests located in `tests/features/test_theapp_features_feature_one.py`
- create a `pyprojects.toml` (content example below) in application base dir and install as a dynamically updated dev package
  - doing this allows you to install the package your are "testing" and import it as if it was a package in your test files.
  - with console in the package directory run equivalent of `pip install -e .`

```ini
# pyprojects.toml
[build-system]
requires = ["hatchling"]
build-backend = "hatchling.build"

[project]
name = "example_package_name"
version = "0.0.1"
```

### Usage Examples:

- `pytest -v`: this will provide a list of test functions executed with increasing percentage values of completion.
- `pytest -k <string to match>`: this will execute all tests that match this string value in the functino name or if it matches the python filename it will execute all tests in that file.
- pytest fixture example usage can be found in `./aux_tests/` dicrectory.
  - [Fixture Readme](./aux_tests/README_fixtures.md)
  - [Autouse Readme](./aux_tests/README_autouse.md)
- export html report (must have pytest-html pip package installed)
  - `pytest --html=report.html`

### Setup pytest configuration file (optional)

While this step is optional, it's recommended to create a pytest.ini or pyproject.toml configuration file in your project directory to specify pytest options. For example, you can configure pytest to discover tests in a specific folder or specify other testing options. Here's an example pytest.ini file:

```ini
[pytest]
testpaths = "tests", "../beta_tests/b2"
addopts = --cov=example_package_name --cov-fail-under=70
```

This configuration tells pytest to discover tests in the "`tests`" and "`../beta_tests/b2`" folder and run code coverage with a minimum coverage threshold of `70%`.

## Debug

To run pytest in debug mode in Visual Studio Code (VSCode), you can use the integrated debugging features. Here's how you can do it:

**Open your Python test file and set breakpoints**:

**Configure the debugging environment**:

- Open the Run and Debug sidebar in VSCode by clicking on the bug icon in the left sidebar or by using the keyboard shortcut `Ctrl+Shift+D` (Windows/Linux) or `Cmd+Shift+D` (macOS).

**Create a Python debugging configuration**:

- In the Run and Debug sidebar, click the gear icon to create a new configuration, and then choose "Python" as the environment. This will create a `launch.json` file in your project's `.vscode` folder.

**Configure the debugging configuration**:

- Open the `launch.json` file and edit it to specify the following:
  - `"program"`: Set this to the path to your pytest executable. You can find the pytest executable in your project's virtual environment's `bin` or `Scripts` directory.
  - `"args"`: Set this to the pytest command-line arguments you want to use, such as the test file or test directory you want to run.
  - Here's an example `launch.json` configuration:

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Pytest",
            "type": "python",
            "request": "launch",
            "program": "${workspaceFolder}/venv/bin/pytest",
            "args": ["tests/your_test_file.py"],
            "cwd": "${workspaceFolder}",
            "console": "integratedTerminal"
        }
    ]
}
```

Make sure to replace `${workspaceFolder}/venv/bin/pytest` with the correct path to your pytest executable and `"tests/your_test_file.py"` with the path to your test file.

**Start debugging**:

With the debugging configuration set up, click the green play button in the Run and Debug sidebar to start debugging pytest. VSCode will run pytest in debug mode and stop at the breakpoints you set.

**Debug your tests**:

You can now use the debugging tools in VSCode to step through your pytest tests, inspect variables, and troubleshoot issues.