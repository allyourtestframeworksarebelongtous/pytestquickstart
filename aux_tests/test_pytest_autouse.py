import pytest

@pytest.fixture
def first_entry():
    """
    Fixture explanation: This fixture initializes 'first_entry' with the string "boxcars".
    """
    return "boxcars"

@pytest.fixture
def order(first_entry):
    """
    Fixture explanation: This fixture initializes an empty list 'order'.
    """
    return []

@pytest.fixture(autouse=True)
def append_first(order, first_entry):
    """
    Fixture explanation: This autouse fixture appends 'first_entry' to 'order'. The autouse=True
    setting ensures that this fixture is automatically used by all tests without explicitly
    needing to request it, allowing 'first_entry' to be added to 'order' in every test case.
    """
    return order.append(first_entry)

def test_string_only(order, first_entry):
    """
    Test explanation: This test checks if the 'order' list contains only the 'first_entry' string.
    """
    assert order == [first_entry]

def test_string_and_int(order, first_entry):
    """
    Test explanation: This test appends an integer to the 'order' list and checks if it contains
    both the 'first_entry' string and the integer.
    """
    order.append(1999)
    assert order == [first_entry, 1999]
