
# Fixture Examples Built into the quick start

In the `test_pytest_fixtures.py` file, we have two sections of fixture examples, `fixtures 1` and `fixtures 2`, along with some test cases that utilize these fixtures.

## Fixtures 1

In this section, we define three fixtures: `order`, `append_first`, and `append_second`. Fixtures are essentially functions that provide reusable setup or data for your tests. Here's what each fixture does:

- `order`: Returns a list `[1, 2, 3]`.
- `append_first`: Appends the number 1 to the `order` list.
- `append_second`: Extends the `order` list with the numbers `[2]`.

Additionally, there is an autouse fixture named `append_third` that appends the number 3 to the `order` list automatically after other fixtures have been applied.

## Fixtures 2

In this section, we define two fixtures: `number_order1` and `number_order2`. These fixtures return lists `[1, 2, 3, 4, 5]` and `[7, 8, 9, 10]` respectively.

There is also an autouse fixture named `combined_num` that combines the contents of `number_order1` and `number_order2`.

## Test Cases

The script includes several test cases that utilize the fixtures we defined above. Here are some examples:

- `test_pystuff`: This test uses the `combined_num` fixture to ensure that the combined list is correctly formed.
- `test_order`: This test uses the `order` fixture to verify that the list is modified as expected.
- `test_fixture1_and_2`: This test integrates both `order` and `combined_num` fixtures to perform multiple assertions.
- `test_fixture1_and_2_FAIL`: This test intentionally fails by asserting incorrect values, demonstrating the failure scenario.
