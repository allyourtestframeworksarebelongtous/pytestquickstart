import pytest

# Define a pytest fixture named 'person'.
# The 'params' argument specifies different sets of input values for the fixture.
# The 'ids' argument provides custom names for each set of values.
@pytest.fixture(params=[("Alice", 25), ("Bob", 30), ("Charlie", 35)], ids=["alice_fixture", "bob_fixture", "charlie_fixture"])
def person(request):
    """
    Fixture explanation: Generates a dictionary representing a person with a name and age.

    Args:
        request: The pytest request object that provides access to parameter values.

    Returns:
        dict: A dictionary containing 'name' and 'age' attributes.
    """
    # Extract the name and age values from the current parameter set.
    name, age = request.param
    # Create a dictionary with 'name' and 'age' attributes and return it.
    return {"name": name, "age": age}

# Test function that checks if the 'name' attribute of the 'person' fixture is a string.
def test_person_name(person):
    """
    Test explanation: Checks if the 'name' attribute of the 'person' fixture is a string.

    Args:
        person: The 'person' fixture, which is a dictionary with 'name' and 'age' attributes.
    """
    assert isinstance(person["name"], str)

# Test function that checks if the 'age' attribute of the 'person' fixture is an integer.
def test_person_age(person):
    """
    Test explanation: Checks if the 'age' attribute of the 'person' fixture is an integer.

    Args:
        person: The 'person' fixture, which is a dictionary with 'name' and 'age' attributes.
    """
    assert isinstance(person["age"], int)
