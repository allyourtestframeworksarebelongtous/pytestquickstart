# pytest Fixture with `params` Argument Example

Lets learn how to use the `params` argument in pytest fixtures to parameterize your tests and run them with different input values. In this example, we create a `person` fixture with multiple sets of values for testing.

### The `person` Fixture

In this example, we create a `person` fixture using the `@pytest.fixture` decorator. The `params` argument is used to define multiple sets of input values for the fixture. Each set is a tuple representing a person's name and age.

```python
@pytest.fixture(params=[("Alice", 25), ("Bob", 30), ("Charlie", 35)], ids=["alice_fixture", "bob_fixture", "charlie_fixture"])
def person(request):
    name, age = request.param
    return {"name": name, "age": age}
```

- The `params` argument contains a list of tuples, where each tuple is a set of values for the fixture.
- The `ids` argument provides custom names for each set of values, making test result output more descriptive.

### Testing the `person` Fixture

We have two test functions that use the `person` fixture:

```python
def test_person_name(person):
    assert isinstance(person["name"], str)

def test_person_age(person):
    assert isinstance(person["age"], int)
```

- `test_person_name` checks if the `name` attribute of the `person` fixture is a string.
- `test_person_age` checks if the `age` attribute of the `person` fixture is an integer.

By running these test functions with different sets of input values specified in the `params` argument, pytest will automatically generate and execute test cases for each set.
