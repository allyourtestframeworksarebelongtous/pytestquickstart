"""
test_pytest is used to test/understand the pytest library
It should always pass and if it doesn't, it implies there is a problem
with how pytest is installed.
"""

import pytest

# fixtures 1
@pytest.fixture
def order():
    """
    Fixture explanation: This fixture initializes an 'order' list with values [1, 2, 3].
    """
    return [1, 2, 3]

@pytest.fixture
def append_first(order):
    """
    Fixture explanation: This fixture appends the value 1 to the 'order' list.
    """
    order.append(1)

@pytest.fixture
def append_second(order, append_first):
    """
    Fixture explanation: This fixture extends the 'order' list with the value 2.
    """
    order.extend([2])

@pytest.fixture(autouse=True)
def append_third(order, append_second):
    """
    Fixture explanation: This fixture adds the value 3 to the 'order' list.
    """
    order += [3]

# fixtures 2
@pytest.fixture
def number_order1():
    """
    Fixture explanation: This fixture initializes 'number_order1' list with values [1, 2, 3, 4, 5].
    """
    return [1, 2, 3, 4, 5]

@pytest.fixture
def number_order2():
    """
    Fixture explanation: This fixture initializes 'number_order2' list with values [7, 8, 9, 10].
    """
    return [7, 8, 9, 10]

@pytest.fixture(autouse=True)
def combined_num(number_order1, number_order2):
    """
    Fixture explanation: This autouse fixture combines 'number_order1' and 'number_order2'
    into 'combined_num' by concatenating them.
    """
    return (number_order1 + number_order2)

# some tests using the fixtures

# This test uses fixture 2 to combine two lists.
def test_pystuff(combined_num):
    """
    Test explanation: This test uses the combined_num fixture, which combines
    number_order1 and number_order2. It checks if the combined list matches
    the expected result.
    """
    assert combined_num == [1, 2, 3, 4, 5, 7, 8, 9, 10]

# This test uses fixture 1 to create a list and append values to it.
def test_order(order):
    """
    Test explanation: This test uses the order fixture, which creates a list and
    appends values to it using fixtures append_first, append_second, and
    append_third. It checks if the final list matches the expected result.
    """
    assert order == [1, 2, 3, 1, 2, 3]

# This test combines fixtures 1 and 2 to perform an integration test.
def test_fixture1_and_2(order, combined_num):
    """
    Test explanation: This test combines fixtures order and combined_num. It
    checks if the combined lists match the expected results and if the sum of
    the two lists is as expected.
    """
    assert combined_num == [1, 2, 3, 4, 5, 7, 8, 9, 10]
    assert order == [1, 2, 3, 1, 2, 3]
    assert order + combined_num == [1, 2, 3, 1, 2, 3] + [1, 2, 3, 4, 5, 7, 8, 9, 10]

# This test is marked as expected to fail.
@pytest.mark.xfail
def test_fixture1_and_2_FAIL(order, combined_num):
    """
    Test explanation: This test is expected to fail intentionally. It checks if
    the combined lists match the expected results, but it intentionally
    includes assertions that will fail.
    """
    assert combined_num == [1, 2, 3, 4, 5, 7, 8, 9, 10]
    assert order == [1, 2, 3]
    assert order + combined_num != [1, 2, 3, 1, 2, 33] + [1, 2, 3, 4, 5, 7, 8, 9, 10]
